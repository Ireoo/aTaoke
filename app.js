// load basic ini
const path = require("path");
const {config, localIP} = require("getlibs.io")(__dirname);

// load web modules
const express = require("express");
const hbs = require("hbs");

// config hbs
require("./SERVER/hbs")(hbs);

// set app from express
const app = express();

// load app
require("./SERVER/app")(app, hbs);

// load routes
require("./SERVER/route")(app);

// load http
http = require("http").Server(app);

// load IO
require("./SERVER/socket")(http);


/**
 * 设置服务器端口默认为80
 */
const server = http.listen(process.env.PORT || 80, () => {
	let s = server.address();
	console.log(`服务器已经启动，绑定地址：${localIP}:${s.port}[${s.family}]`);
});


// const {execSync} = require("child_process");
// process.on("SIGINT", function () {
// 	execSync(`rm -rf ${path.join(process.cwd(), "sessions")}`);
// 	process.exit();
// });
