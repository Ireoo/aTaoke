const {spawn} = require("child_process");
const fs = require("fs");

exports = module.exports = (IO, command) => {
	return new Promise((res, err) => {
		let e = spawn(`node`, [command]);
		
		e.stdout.on("data", (data) => {
			let datas = data.toString().split(/\r|\n/g);
			datas.map(v => {
				if (v !== "") {
					if (/^[\[{]+(.*?)[\]}]+$/g.test(v)) {
						for (let i in IO) {
							IO[i].emit("console", JSON.parse(v));
						}
					} else {
						console.log(v);
					}
				}
			});
		});
		
		e.stderr.on("data", (data) => {
			let datas = data.toString().split(/\r|\n/g);
			datas.map(v => {
				if (v !== "") {
					if (/^[\[{]+(.*?)[\]}]+$/g.test(v)) {
						for (let i in IO) {
							IO[i].emit("console", JSON.parse(v));
						}
					} else {
						console.log(v);
					}
				}
			});
		});
		
		e.on("close", (code) => {
			res(code);
		});
		
	});
};
