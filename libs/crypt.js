const crypto = require("crypto");

// use secret to encrypt string
function encrypt(str, secret) {
	let cipher = crypto.createCipher("aes192", secret);
	let enc = cipher.update(str, "utf8", "hex");
	enc += cipher.final("hex");
	// console.log("enc is: " + enc);
	return enc;
}

//use secret to decrypt string
function decrypt(str, secret) {
	let decipher = crypto.createDecipher("aes192", secret);
	let dec = decipher.update(str, "hex", "utf8");
	dec += decipher.final("utf8");
	// console.log("dec is: " + dec);
	return dec;
}

exports = module.exports = {
	encrypt,
	decrypt
};
