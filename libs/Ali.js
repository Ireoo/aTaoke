const Config = require("./config").config;
const time = require("./time");
const random = require('./random');
const ApiClient = require("../API/AliSDK/index.js").ApiClient;

const {defaults} = require("lodash");

const client = new ApiClient(
	{
		"appkey": Config.Ali.appkey,
		"appsecret": Config.Ali.appsecret,
		"url": Config.Ali.url
	}
);


// 设置部分参数去访问淘宝指定KEY的API
const exec = (key, post) => {
	return new Promise((resolve, reject) => {
		client.execute(key, post, function (error, response) {
			if (!error) resolve(response);
			else reject(`\r\nAlimama: USE  -> ${key}\r\n         POST -> ${JSON.stringify(post)}\r\n         GET  -> ${JSON.stringify(error)}\r\n         http://open.taobao.com/apitools/errorCodeSearch`);
		});
	});
};

exports = module.exports = {
	
	Ali: {
		
		exec: exec,
		
		// 实例化：获取后台供卖家发布商品的标准商品类目
		taobao_itemcats_get: (params = {}) => {
			let post = {
				// 商品所属类目ID列表，用半角逗号(,)分隔 例如:(18957,19562,) (cids、parent_cid至少传一个)
				// 'cids': '18957,19562',
				// 'datetime': '2000-01-01 00:00:00',
				
				// 需要返回的字段列表，见ItemCat，默认返回：cid,parent_cid,name,is_parent；增量类目信息,根据fields传入的参数返回相应的结果。 features字段： 1、如果存在attr_key=freeze表示该类目被冻结了，attr_value=0,5，value可能存在2个值（也可能只有1个），用逗号分割，0表示禁编辑，5表示禁止发布
				"fields": "cid,parent_cid,name,is_parent",
				
				// 父商品类目 id，0表示根节点, 传输该参数返回所有子类目。 (cids、parent_cid至少传一个)
				"parent_cid": "0"
			};
			post = defaults(params, post);
			return exec("taobao.itemcats.get", post);
		},
		
		// 实例化：淘宝客商品查询
		taobao_tbk_item_get: (params = {}) => {
			let post = {
				// 需返回的字段列表 *
				"fields": "num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url,seller_id,volume,nick",
				
				// 查询词 *
				"q": params.keyword,
				
				// 	后台类目ID，用,分割，最大10个，该ID可以通过taobao.itemcats.get接口获取到 *
				// 'cat': '16, 18',
				
				// 	所在地
				// 'itemloc': '杭州',
				
				// 排序_des（降序），排序_asc（升序），销量（total_sales），淘客佣金比率（tk_rate）， 累计推广量（tk_total_sales），总支出佣金（tk_total_commi）
				"sort": "tk_rate",
				
				// 是否商城商品，设置为true表示该商品是属于淘宝商城商品，设置为false或不设置表示不判断这个属性
				// 'is_tmall': 'false',
				
				// 是否海外商品，设置为true表示该商品是属于海外商品，设置为false或不设置表示不判断这个属性
				// 'is_overseas': 'false',
				
				// 折扣价范围下限，单位：元
				// 'start_price': '10',
				
				// 折扣价范围上限，单位：元
				// 'end_price': '10',
				
				// 淘客佣金比率上限，如：1234表示12.34%
				// 'start_tk_rate': '1234',
				
				// 淘客佣金比率下限，如：1234表示12.34%
				"end_tk_rate": 1234,
				
				// 链接形式：1：PC，2：无线，默认：１
				// 'platform': '1',
				
				// 第几页，默认：１
				// 'page_no': '1',
				
				// 页大小，默认20，1~100
				"page_size": 100
			};
			post = defaults(params, post);
			return exec("taobao.tbk.item.get", post);
		},
		
		// 实例化：淘宝客商品链接转换
		taobao_tbk_item_convert: (params = {}) => {
			let post = {
				// 需返回的字段列表 *
				"fields": "num_iid,click_url",
				
				// 商品ID串，用','分割，从taobao.tbk.item.get接口获取num_iid字段，最大40个 *
				// 'num_iids': '123,456',
				
				// 广告位ID，区分效果位置 *
				"adzone_id": Config.Ali.adzone_id,
				
				// 链接形式：1：PC，2：无线，默认：１
				// 'platform': '123',
				
				// 自定义输入串，英文和数字组成，长度不能大于12个字符，区分不同的推广渠道
				// 'unid': 'demo',
				
				// 1表示商品转通用计划链接，其他值或不传表示转营销计划链接
				// 'dx': '1'
			};
			post = defaults(params, post);
			return exec("taobao.tbk.item.convert", post);
		},
		
		// 实例化：淘宝客商品关联推荐查询
		taobao_tbk_item_recommend_get: (params = {}) => {
			let post = {
				// 需返回的字段列表
				"fields": "num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url",
				
				// 商品Id
				"num_iid": "123",
				
				// 返回数量，默认20，最大值40
				"count": "20",
				
				// 链接形式：1：PC，2：无线，默认：１
				"platform": "1"
			};
			post = defaults(params, post);
			return exec("taobao.tbk.item.recommend.get", post);
		},
		
		// 实例化：淘宝客商品详情（简版）
		taobao_tbk_item_info_get: (params = {}) => {
			let post = {
				// 需返回的字段列表
				"fields": "num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url",
				
				// 链接形式：1：PC，2：无线，默认：１
				// 'platform': '1',
				
				// 商品ID串，用,分割，从taobao.tbk.item.get接口获取num_iid字段，最大40个
				// 'num_iids': '123,456'
			};
			post = defaults(params, post);
			return exec("taobao.tbk.item.info.get", post);
		},
		
		// 实例化：淘宝客返利授权查询
		taobao_tbk_rebate_auth_get: (params = {}) => {
			let post = {
				// 需返回的字段列表
				"fields": "param,rebate",
				
				// nick或seller_id或num_iid
				// 'params': 'demo',
				
				// 类型：1-按nick查询，2-按seller_id查询，3-按num_iid查询
				"type": "3"
			};
			post = defaults(params, post);
			return exec("taobao.tbk.rebate.auth.get", post);
		},
		
		// 实例化：淘宝客返利订单查询
		taobao_tbk_rebate_order_get: (params = {}) => {
			let post = {
				// 需返回的字段列表
				"fields": "tb_trade_parent_id,tb_trade_id,num_iid,item_title,item_num,price,pay_price,seller_nick,seller_shop_title,commission,commission_rate,unid,create_time,earning_time",
				
				// 订单结算开始时间
				"start_time": time.format(),
				
				// 订单查询时间范围,单位:秒,最小60,最大600,默认60
				"span": 600,
				
				// 第几页，默认1，1~100
				"page_no": 1,
				
				// 页大小，默认20，1~100
				"page_size": 100
			};
			post = defaults(params, post);
			return exec("taobao.tbk.rebate.order.get", post);
		},
		
		
		// 实例化：淘宝客淘口令
		taobao_tbk_tpwd_create: (params = {}) => {
			let post = {
				// 生成口令的淘宝用户ID
				"user_id": "123",
				
				// 口令弹框内容
				"text": "长度大于5个字符",
				
				// 口令跳转目标页
				"url": "https://uland.taobao.com/",
				
				// 口令弹框logoURL
				"logo": "https://uland.taobao.com/",
				
				// 扩展字段JSON格式
				"ext": "{}"
			};
			post = defaults(params, post);
			return exec("taobao.tbk.tpwd.create", post);
		},
		
		// 实例化：淘宝客广告位创建API
		taobao_tbk_adzone_create: (params = {}) => {
			let post = {
				// 网站ID
				"site_id": Config.Ali.site_id,
				
				// 广告位名称，最大长度64字符
				"adzone_name": `${time.format("YYYYMMDDHHmmss")}-${random()}`
			};
			post = defaults(params, post);
			return exec("taobao.tbk.adzone.create", post);
		},
		
		// 实例化：获取淘宝联盟选品库列表
		taobao_tbk_uatm_favorites_get: (params = {}) => {
			let post = {
				// 第几页，从1开始计数
				"page_no": "1",
				
				// 默认20，页大小，即每一页的活动个数
				"page_size": "200",
				
				// 	需要返回的字段列表，不能为空，字段名之间使用逗号分隔
				"fields": "favorites_title,favorites_id,type",
				
				// 默认值-1；选品库类型，1：普通选品组，2：高佣选品组，-1，同时输出所有类型的选品组
				"type": "-1"
			};
			post = defaults(params, post);
			return exec("taobao.tbk.uatm.favorites.get", post);
		},
		
		// 实例化：获取淘宝联盟选品库的宝贝信息
		taobao_tbk_uatm_favorites_item_get: (params = {}) => {
			let post = {
				// 链接形式：1：PC，2：无线，默认：１
				"platform": "1",
				
				// 页大小，默认20，1~100
				"page_size": "100",
				
				// 推广位id，需要在淘宝联盟后台创建；且属于appkey备案的媒体id（siteid），如何获取adzoneid，请参考，http://club.alimama.com/read-htm-tid-6333967.html?spm=0.0.0.0.msZnx5
				"adzone_id": Config.Ali.adzone_id,
				
				// 自定义输入串，英文和数字组成，长度不能大于12个字符，区分不同的推广渠道
				"unid": "API",
				
				// 选品库的id
				"favorites_id": "10010",
				
				// 第几页，默认：1，从1开始计数
				"page_no": "1",
				
				// 需要输出则字段列表，逗号分隔
				"fields": "num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url,click_url,coupon_click_url,seller_id,volume,nick,shop_title,zk_final_price_wap,event_start_time,event_end_time,tk_rate,status,type"
			};
			post = defaults(params, post);
			return exec("taobao.tbk.uatm.favorites.item.get", post);
		},
		
		// 实例化：枚举正在进行中的定向招商的活动列表
		taobao_tbk_uatm_event_get: (params = {}) => {
			let post = {
				// 默认1，第几页，从1开始计数
				"page_no": "1",
				
				// 默认20, 页大小，即每一页的活动个数
				"page_size": "20",
				
				// 需要返回的字段列表，不能为空，字段名之间使用逗号分隔
				"fields": "event_id,event_title,start_time,end_time"
			};
			post = defaults(params, post);
			return exec("taobao.tbk.uatm.event.get", post);
		},
		
		// 实例化：聚划算商品搜索接口
		taobao_ju_items_search: (params = {}) => {
			let post = {
				"param_top_item_query": JSON.stringify(
					{
						current_page: 1,
						page_size: 100,
						pid: Config.Ali.pid,
						word: params.keyword
					}
				)
			};
			post = defaults(params, post);
			return exec("taobao.ju.items.search", post);
		}
	}
};
