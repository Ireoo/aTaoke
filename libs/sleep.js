exports = module.exports = timer => {
	return new Promise((res, req) => {
		setTimeout(function () {
			console.log(`Wait ${timer}MS.`);
			res();
		}, timer);
	});
};
