// load system modules
const fs = require("fs");
const path = require("path");
const crypt = require("./crypt");

const WEB = path.join(__dirname, "..");
const ROOT = process.cwd();

if (!fs.existsSync(path.join(ROOT, "DATA"))) fs.mkdirSync(path.join(ROOT, "DATA"));
const configDefaultFile = path.join(WEB, "WEB/temp", "config.default.json");
const configFile = path.join(ROOT, "DATA", ".config");

// load config from config.json
exports = module.exports = {
	config: (() => {
		let config = {};
		if (fs.existsSync(configFile)) {
			config = JSON.parse(crypt.decrypt(fs.readFileSync(configFile, "utf-8"), "iTaoke"));
		} else {
			let txt = fs.readFileSync(configDefaultFile, "utf-8");
			fs.writeFileSync(configFile, crypt.encrypt(txt, "iTaoke"));
			config = JSON.parse(crypt.decrypt(fs.readFileSync(configFile, "utf-8"), "iTaoke"));
		}
		return config;
	})(),
	configSet: (config) => {
		try {
			fs.writeFileSync(configFile, crypt.encrypt(JSON.stringify(config, null, 4), "iTaoke"));
			this.config = config;
			return true;
		} catch (e) {
			return false;
		}
	}
};
