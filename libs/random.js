exports = module.exports = (dig = 4) => {
	return Math.floor(Math.random() * (Math.pow(10, dig) - Math.pow(10, (dig - 1)))) + Math.pow(10, (dig - 1));
};
