// load system modules
const fs = require("fs");
const path = require("path");
const ROOT = path.join(__dirname, '..');

const {config} = require("./config");
let themeDir = "default";
if (config.site.theme && config.site.theme !== "") themeDir = config.site.theme;

function Route(app) {
	// basic
	this._app = app;
	this._type = "all";
	
	// function
	this.format = (address) => {
		let fileName = [];
		if (address !== "/") {
			let addressArray = address.split("/");
			addressArray.map((v, i) => {
				if (v !== "") {
					if (!/^:/.test(v)) {
						fileName.push(v);
					} else {
						fileName.push(v.replace(":", ""));
					}
				}
			});
		} else {
			fileName.push("index");
		}
		return fileName.join("/");
	};
	
	// main
	this.type = (type) => {
		this._type = type;
		return this;
	};

	this.use = (cb) => {
		if(this._app) {
			this._app.use((req, res, next) => {
				cb(req, res, next);
			});
		} else {
			cb();
		}
		return this;
	};
	
	this.route = (address, create = false) => {
		if (this._app) {
			let file = this.format(address);
			let filePath = path.join(ROOT, "WEB/route/", `${file}.js`);
			
			let fileExist = fs.existsSync(filePath);
			if (fileExist) this._app[this._type](address, require(filePath)[this._type]);
			console.warn(`路由规则 -> [${this._type}]${this._type.length < 4 ? " " : ""} 加载路由 ${file}.js 文件${fileExist ? "成功" : "失败"}！`);
			if (create && !fileExist) {
				if (this.createRoute(address)) this.createPage(address);
			}
		} else {
			console.warn(`加载服务端失败！`);
		}
		return this;
	};
	
	this.createRoute = (address) => {
		// get file path without ext
		let file = this.format(address);
		let filePath = path.join(ROOT, "WEB/route", `${file}.js`);
		
		// get route temp
		let defaultRoute = fs.readFileSync(path.join(ROOT, `WEB/temp/route/index.js`), "utf-8");
		defaultRoute = defaultRoute.replace(/\{file\}/g, file);
		
		// 分解file：前面为路径，最后一个为文件名
		let fileArray = file.split("/");
		let fileName = fileArray.pop();
		let dirName = fileArray.join("/");
		
		// 获取完整路径
		let dir = path.join(ROOT, "WEB/route", dirName);
		
		// 目录不存在就创建
		if (!fs.existsSync(dir)) fs.mkdirSync(dir);
		
		// 文件是否存在
		if (!fs.existsSync(filePath)) {
			//写入文件
			let createFile = fs.writeFileSync(filePath, defaultRoute);
			console.warn(`         -> 创建路由 ${file}.js 文件${createFile ? "失败" : "成功"}！`);
			return !createFile;
		} else {
			console.warn(`         -> 创建网页 ${file}.js 文件失败！文件已经存在！`);
			return true;
		}
	};
	
	this.createPage = (address) => {
		// get file path without ext
		let file = this.format(address);
		let filePath = path.join(ROOT, "WEB/html", themeDir, `${file}.hbs`);
		
		// get route temp
		let defaultPage = fs.readFileSync(path.join(ROOT, `WEB/temp/route/index.hbs`), "utf-8");
		
		// 分解file：前面为路径，最后一个为文件名
		let fileArray = file.split("/");
		let fileName = fileArray.pop();
		let dirName = fileArray.join("/");
		
		// 获取完整路径
		let dir = path.join(ROOT, "WEB/html", themeDir, dirName);
		
		// 目录不存在就创建
		if (!fs.existsSync(dir)) fs.mkdirSync(dir);
		
		// 文件是否存在
		if (!fs.existsSync(filePath)) {
			//写入文件
			let createFile = fs.writeFileSync(filePath, defaultPage);
			console.warn(`         -> 创建网页 ${file}.html 文件${createFile ? "失败" : "成功"}！`);
			return !createFile;
		} else {
			console.warn(`         -> 创建网页 ${file}.html 文件失败！文件已经存在！`);
			return true;
		}
		
	};
};

exports = module.exports = Route;
