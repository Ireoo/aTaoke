const Config = require("./config").config;
const Api = require("iapi");

function DB(config = Config.database) {
	this.API = Api;
	this.API.config = {
		url: process.env.API_URL || config.url,
		key: process.env.API_KEY || config.key
	};
	this._data = {
		where: {},
		data: {},
		other: {}
	};
	
	this.table = (table) => {
		this._table = table;
		return this;
	};
	
	this.where = (where) => {
		this._data.where = where || {};
		return this;
	};
	
	this.data = (data) => {
		this._data.data = data || {};
		return this;
	};
	
	this.other = (other) => {
		this._data.other = other || {};
		return this;
	};
	
	this.get = (command, cb) => {
		let self = this;
		if (Config.debug) console.log(`SQL      -> [into]  ${JSON.stringify(this._data)}`);
		self.API.api(`${self._table}/${command}`, self._data, (err, data) => {
			self._data = {
				where: {},
				data: {},
				other: {}
			};
			cb(err, data);
			if (Config.debug) {
				if (!err) {
					console.log(`SQL      -> [info]  ${JSON.stringify(data)}`);
				} else {
					console.log(`SQL      -> [error] ${JSON.stringify(err)}`);
				}
			}
		});
	};
	
	this.getSync = (command) => {
		let self = this;
		return new Promise((resolve, reject) => {
			self.get(command, (err, data) => {
				if (err) reject(err);
				else resolve(data);
			});
		});
	};
}

exports = module.exports = {
	DB: DB
};
