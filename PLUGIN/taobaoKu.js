// load system modules
const fs = require("fs");
const path = require("path");

// load libs modules
const { config, DB, Ali, time } = require("getlibs.io")(path.join(__dirname, '..'));
let success = false, failTime = 3;
(async () => {
	const db = new DB();
	
	for (let f = 1; f <= 2; f++) {
		failTime = 3;
		let lists = [];
		do {
			success = false;
			try {
				console.log(JSON.stringify(
					{
						type: "taobaoKu",
						status: "warn",
						success: true,
						message: `尝试获取第 ${f} 页淘宝库列表...`,
						timer: time.format()
					}
				));
				lists = (await Ali.taobao_tbk_uatm_favorites_get({page_no: f})).results.tbk_favorites;
				success = true;
			} catch (err) {
				console.log(JSON.stringify(
					{
						type: "taobaoKu",
						status: "error",
						success: false,
						message: `获取第 ${f} 页淘宝库列表失败! 错误代码：${JSON.stringify(err)}`,
						timer: time.format()
					}
				));
				failTime--;
			}
			;
		} while (!success && failTime >= 0);
		console.log(JSON.stringify(
			{
				type: "taobaoKu",
				status: "warn",
				success: true,
				message: `列表获取完毕,共获取商品库 ${lists.length} 个!`,
				timer: time.format()
			}
		));
		for (let i = 0; i < lists.length; i++) {
			let list = lists[i];
			for (let c = 1; c <= 2; c++) {
				failTime = 3;
				let datas = [];
				do {
					success = false;
					try {
						console.log(JSON.stringify(
							{
								type: "taobaoKu",
								status: "warn",
								success: true,
								message: `[ID: ${list.favorites_id}] "${list.favorites_title}" 尝试获取宝贝列表...`,
								timer: time.format()
							}
						));
						datas = (await Ali.taobao_tbk_uatm_favorites_item_get(
							{
								favorites_id: list.favorites_id,
								adzone_id: config.Ali.adzone_id,
								page_no: c
							}
						)).results.uatm_tbk_item;
						success = true;
					} catch (err) {
						console.log(JSON.stringify(
							{
								type: "taobaoKu",
								status: "error",
								success: false,
								message: `[ID: ${list.favorites_id}] "${list.favorites_title}" 获取宝贝列表失败! 错误代码：${JSON.stringify(err)}`,
								timer: time.format()
							}
						));
						failTime--;
					}
					;
				} while (!success && failTime >= 0);
				// console.log(datas.results.uatm_tbk_item.length);
				for (let d = 0; d < datas.length; d++) {
					let v = datas[d];
					failTime = 3;
					let one = {};
					do {
						success = false;
						try {
							console.log(JSON.stringify(
								{
									type: "taobaoKu",
									status: "warn",
									success: true,
									message: `[ID: ${v.num_iid}] "${v.title}" 尝试保存数据...`,
									timer: time.format()
								}
							));
							v.updateTime = time.now();
							one = await db.table("items").where({num_iid: v.num_iid}).data({$set: v}).other({upsert: true}).getSync("update");
							success = true;
							if (one.success) {
								r = `[ID: ${v.num_iid}] "${v.title}" 数据更新成功!`;
								console.log(JSON.stringify(
									{
										type: "taobaoKu",
										status: "ok",
										success: true,
										message: r,
										timer: time.format()
									}
								));
							} else {
								r = `[ID: ${v.num_iid}] "${v.title}" 数据保存失败! 错误代码: ${one.data}!`;
								console.log(JSON.stringify(
									{
										type: "taobaoKu",
										status: "error",
										success: false,
										message: r,
										timer: time.format()
									}
								));
							}
						} catch (err) {
							let r = `[ID: ${v.num_iid}] "${v.title}" 数据保存失败! 错误代码：${JSON.stringify(err)}`;
							console.log(JSON.stringify(
								{
									type: "taobaoKu",
									status: "error",
									success: false,
									message: r,
									timer: time.format()
								}
							));
							failTime--;
						}
						;
						
					} while (!success && failTime >= 0);
				}
				
			}
		}
	}
	
})();
