const fs = require("fs");
const path = require("path");
const { DB, ROOT, time } = require("getlibs.io")(path.join(__dirname, '..'));
const web_login = require("nightmare")(
	{
		show: true
	}
);
const web_get = require("nightmare")(
	{
	    show: true
	}
);
const db = new DB();

let COOKIES = null;

// if (fs.existsSync('./cookies.json')) COOKIES = JSON.parse(fs.readFileSync('./cookies.json', 'utf-8'));

function login() {
	if (COOKIES === null) {
		web_login
			.goto("https://login.taobao.com/member/login.jhtml?style=mini&newMini2=true&from=alimama&redirectURL=http%3a%2f%2fpub.alimama.com%2fpromo%2fsearch%2findex.htm")
			.wait("a.J_Quick2Static")
			.click("a.J_Quick2Static")
			.type("#TPL_username_1", "chaochaowo1")
			.type("#TPL_password_1", "ccalaop1314")
			.visible("#nc_1_n1z")
			.then(ex => {
				if (!ex) {
					console.log(`不需要验证条`);
					return web_login
						.click("#J_SubmitStatic")
						.wait(".logo")
						.cookies.get();
				} else {
					console.log(`需要验证条, 延迟10秒后重试！`);
					return web_login
						.wait(10000)
						.wait("#nc_1_n1z")
						.click("#J_SubmitStatic")
						.wait(".logo")
						.goto(`http://pub.alimama.com/promo/search/index.htm`)
						.cookies.get();
				}
			})
			.then(cookie => {
				COOKIES = cookie;
				if (!fs.existsSync(path.join(ROOT, "DATA"))) fs.mkdirSync(path.join(ROOT, "DATA"));
				fs.writeFileSync(path.join(ROOT, "DATA", "cookies.json"), JSON.stringify(
					{
						COOKIES: COOKIES,
						time: time.now()
					}
				));
				return web_login.end();
			}).then(() => {
			getCookies();
		});
	} else {
		getCookies();
	}
}

function getCookies(IO) {
	if (!fs.existsSync(path.join(ROOT, "DATA"))) fs.mkdirSync(path.join(ROOT, "DATA"));
	if (fs.existsSync(path.join(ROOT, "DATA", "cookies.json"))) {
		if(IO) IO.emit("console", {
			type: "TBK.cookies.start",
			success: true,
			message: `cookies内容存在，正在配置系统...`,
			timer: time.format()
		});
		try {
			config = JSON.parse(fs.readFileSync(path.join(ROOT, "DATA", "cookies.json"), "utf-8"));
			if (config.time && config.time > (time.now() - 60 * 1000 * 15)) {
				console.log(`start set cookie...`);
				web_get
					.then(() => {
						let cookie = config.COOKIES;
						for (let c in cookie) {
							cookie[c].url = "http://pub.alimama.com/promo/search/index.htm";
						}
						return web_get
							.cookies.set(cookie)
							.cookies.get();
					})
					.then(c => {
						console.log(`start set cookie is OK.`);
						if(IO) IO.emit("console", {
							type: "TBK.cookies.set",
							success: true,
							message: `cookies内容获取成功!正在启动主体程序...`,
							timer: time.format()
						});
						get(IO);
					}).catch(e => {
						if(IO) IO.emit("console", {
						type: "TBK.cookies.catch",
						success: false,
						message: `cookies内容配置未知错误, 错误信息: ${e}`,
						timer: time.format()
					});
				});
			} else {
				if(IO) IO.emit("console", {
					type: "TBK.cookies.time",
					success: false,
					message: `cookies已经过期,请先更新cookies内容!当前时间戳：${time.now()}`,
					timer: time.format()
				});
			}
		} catch (e) {
			if(IO) IO.emit("console", {
				type: "TBK.cookies.get",
				success: false,
				message: `cookies获取失败，错误信息: ${e}`,
				timer: time.format()
			});
		}
	} else {
		if(IO) IO.emit("console", {
			type: "TBK.cookies.set",
			success: false,
			message: `cookies内容不存在,请先更新cookies内容!`,
			timer: time.format()
		});
	}
}

function get(IO) {
	db.table("items").where({tbk: {$exists: false}}).other(
		{
			show: {
				_id: 1,
				title: 1,
				num_iid: 1,
				item_url: 1
			}
		}
	).getSync("findone").then(data => {
		if(IO) IO.emit("console", {type: "TBK.get.start", success: true, message: JSON.stringify(data), timer: time.format()});
		web_get
			.then(() => {
				return web_get
					.cookies.get();
			})
			.then(COOKIES => {
				fs.writeFileSync(path.join(ROOT, "DATA", "cookies.json"), JSON.stringify(
					{
						COOKIES: COOKIES,
						time: time.now()
					}
				));
				return web_get;
			})
			.then(ex => {
				return web_get
					.goto(`http://pub.alimama.com/promo/search/index.htm?queryType=2&q=${encodeURIComponent(data.item_url)}`)
					.wait(2000)
					.exists("#J_item_list div.no-data");
			})
			.then(ex => {
				if (!ex) {
					return web_get
						.wait("#J_search_results > div > div > div.box-btn-group > a.box-btn-left")
						.click("#J_search_results > div > div > div.box-btn-group > a.box-btn-left")
						.wait("#zone-form > div:nth-child(5) > div > button")
						.click("#zone-form > div:nth-child(5) > div > button")
						.click("#zone-form > div:nth-child(5) > div > div > ul > li:nth-child(2) > a")
						.wait("#J_global_dialog > div > div.dialog-ft > button.btn.btn-brand.w100.mr10")
						.click("#J_global_dialog > div > div.dialog-ft > button.btn.btn-brand.w100.mr10")
						
						.wait("#magix_vf_code > div > div.dialog-hd > ul > li:nth-child(2)")
						.click("#magix_vf_code > div > div.dialog-hd > ul > li:nth-child(2)")
						
						.exists("#clipboard-target-1")
						.then(ex => {
							if (ex) {
								return web_get
									.evaluate(function () {
										return {
											tbk: document.querySelector("#clipboard-target-1").innerHTML,
											unm: document.querySelector("#clipboard-target-2").innerHTML
										};
									});
							} else {
								return web_get
									.evaluate(function () {
										return {
											tbk: document.querySelector("#clipboard-target").innerHTML,
											unm: null
										};
									});
							}
						});
				} else {
					return web_get
						.evaluate(function () {
							return {
								tbk: null,
								unm: null
							};
						});
				}
			})
			.then(async result => {
				console.log(result);
				if (result) {
					let r = await db.where({_id: data._id}).data({$set: result}).getSync("update");
					if(IO) IO.emit("console", {
						type: "TBK.db.update",
						success: true,
						message: JSON.stringify(result),
						timer: time.format()
					});
				} else {
					let r = await db.where({_id: data._id}).getSync("remove");
					if(IO) IO.emit("console", {
						type: "TBK.db.remove",
						success: false,
						message: JSON.stringify(result),
						timer: time.format()
					});
				}
				get(IO);
			})
			.catch(error => {
				if(IO) IO.emit("console", {
					type: "TBK.get.catch",
					success: false,
					message: JSON.stringify(error),
					timer: time.format()
				});
				get(IO);
			});
	});
}

exports = module.exports = {
	login: login,
	start: getCookies,
	stop: (IO) => {
		web_get
			.end()
			.then(() => {
				IO.emit("console", {type: "TBK.stop", success: true, message: `淘宝客链接转化已停止!`, timer: time.format()});
			})
			.catch(error => {
				IO.emit("console", {type: "TBK.stop.catch", success: false, message: error, timer: time.format()});
			});
	}
};
