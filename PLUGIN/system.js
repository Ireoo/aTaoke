const _ = require("lodash");
const ps = require("current-processes");
const os = require("os");

const getSystem = (IO, data = {cpus: 0, rams: 0, topram: "0M"}) => {
	ps.get((err, processes) => {
		
		let sorted = _.sortBy(processes, "cpu");
		let top5 = sorted.reverse();
		let cpus = 0, rams = 0;
		top5.forEach(v => {
			// if (v.name === 'node') {
			cpus += v.cpu;
			rams += v.mem.usage;
			// }
		});
		let tram = os.totalmem() / 1024 / 1024,
			tram_ = "M";
		if (tram > 1024) {
			tram = tram / 1024;
			tram_ = "G";
		}
		if (tram > 1024) {
			tram = tram / 1024;
			tram_ = "T";
		}
		const _data = {
			cpus: cpus,
			rams: Math.round(rams),
			topram: Math.round(tram) + tram_
		};
		if (data.cpus !== _data.cpus || data.rams !== _data.rams || data.topram !== _data.topram) {
			for (let i in IO) {
				IO[i].emit("system", _data);
			}
		}
		setTimeout(() => {
			getSystem(IO);
		}, 1000);
	});
};

exports = module.exports = getSystem;
