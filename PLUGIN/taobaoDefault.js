// load system modules
const fs = require("fs");
const path = require("path");
const async = require("async");

// load libs modules
const { config, DB, Ali, time } = require("getlibs.io")(path.join(__dirname, '..'));

(async () => {
	const db = new DB();
	let list = await db.table("gather").getSync("find");
	// list = [{ q: '女装' }];
	if (list.length > 0) {
		for (let l = 0; l < list.length; l++) {
			let post = list[l];
			if (post._id) delete post._id;
			let now = 0;
			let top = 1000;
			do {
				post.page_no = now + 1;
				try {
					let result = await Ali.taobao_tbk_item_get(post);
					let data = result.results.n_tbk_item;
					for (let i = 0; i < data.length; i++) {
						try {
							let v = data[i];
							v.updateTime = time.now();
							let one = await db.table("items").where({num_iid: v.num_iid}).data({$set: v}).other({upsert: true}).getSync("update");
							if (one.ok === 1) {
								r = `[ID: ${v.num_iid}] "${v.title}" 数据更新成功!`;
								console.log(JSON.stringify(
									{
										type: "taobaoItem",
										success: true,
										message: r,
										timer: time.format()
									}
								));
							} else {
								r = `[ID: ${v.num_iid}] "${v.title}" 数据保存失败! 未知错误代码!`;
								console.log(JSON.stringify(
									{
										type: "taobaoItem",
										success: false,
										message: r,
										timer: time.format()
									}
								));
							}
						} catch (err) {
							let r = `[ID: ${v.num_iid}] "${v.title}" 数据保存失败! 错误代码：${JSON.stringify(err)}`;
							console.log(JSON.stringify(
								{
									type: "taobaoItem",
									success: false,
									message: r,
									timer: time.format()
								}
							));
						}
						;
					}
					;
					top = result.total_results % 100 === 0 ? result.total_results : result.total_results + (100 - result.total_results % 100);
					now++;
				} catch (err) {
					let r = `"${post.keyword}" 数据获取失败! 错误代码：${JSON.stringify(err)}`;
					console.log(JSON.stringify({type: "taobaoItem", success: false, message: r, timer: time.format()}));
				}
				;
			} while (top > now * 100);
		}
		;
	} else {
		console.log(JSON.stringify(
			{
				type: "taobaoItem",
				success: false,
				message: `暂时还没有需要采集的资源!`,
				timer: time.format()
			}
		));
	}
})();
