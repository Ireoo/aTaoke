// load system modules
// const fs = require("fs");
// const path = require("path");

// load libs modules
const path = require('path');
const { config, DB, Ali, time } = require("getlibs.io")(path.join(__dirname, '..'));

(async (pid) => {
	const db = new DB();
	let list = await db.table("gather").getSync("find");
	// list = [{ q: '女装' }];
	if (list.length > 0) {
		for (let l = 0; l < list.length; l++) {
			let post = list[l];
			if (post._id) delete post._id;
			let now = 0;
			let top = 1000;
			do {
				post.page_no = now + 1;
				let param_top_item_query = JSON.stringify(
					{
						current_page: post.page_no,
						page_size: 100,
						pid: pid || config.Ali.pid
					}
				);
				let results = await Ali.taobao_ju_items_search({param_top_item_query});
				console.log(param_top_item_query);
				let data = results.result.model_list.items;
				for (let i = 0; i < data.length; i++) {
					try {
						let v = data[i];
						v.updateTime = time.now();
						let one = await db.table("polys").where({item_id: v.item_id}).data({$set: v}).other({upsert: true}).getSync("update");
						if (one.ok === 1) {
							r = `[ID: ${v.item_id}] "${v.title}" 数据更新成功!`;
							console.log(JSON.stringify(
								{
									type: "taobaoPoly",
									success: true,
									message: r,
									timer: time.format()
								}
							));
						} else {
							r = `[ID: ${v.item_id}] "${v.title}" 数据保存失败! 未知错误代码!`;
							console.log(JSON.stringify(
								{
									type: "taobaoPoly",
									success: false,
									message: r,
									timer: time.format()
								}
							));
						}
					} catch (err) {
						let r = `[ID: ${v.item_id}] "${v.title}" 数据保存失败! 错误代码：${JSON.stringify(err)}`;
						console.log(JSON.stringify(
							{
								type: "taobaoPoly",
								success: false,
								message: r,
								timer: time.format()
							}
						));
					}
					;
				}
				;
				top = results.result.total_item % 100 === 0 ? results.result.total_item : results.result.total_item + (100 - results.result.total_item % 100);
				now++;
			} while (top > now * 100);
		}
		;
	} else {
		console.log(JSON.stringify(
			{
				type: "taobaoPoly",
				success: false,
				message: `暂时还没有需要采集的资源!`,
				timer: time.format()
			}
		));
	}
})();
