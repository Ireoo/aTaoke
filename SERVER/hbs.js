const path = require("path");
const { ROOT } = require("getlibs.io")(path.join(__dirname, '..'));
const _ = require("lodash");

exports = module.exports = hbs => {
	var blocks = {};
	
	hbs.registerPartials(path.join(ROOT, "WEB/html/template"));
	
	hbs.registerHelper("extend", (name, context) => {
		var block = blocks[name];
		if (!block) {
			block = blocks[name] = [];
		}
		
		block.push(context.fn(this)); // for older versions of handlebars, use block.push(context(this));
	});
	
	hbs.registerHelper("block", (name, context) => {
		var len = (blocks[name] || []).length;
		var val = (blocks[name] || []).join("\n");
		
		// clear the block
		blocks[name] = [];
		
		return len ? val : context.fn ? context.fn(this) : "";
	});
	
	hbs.registerHelper("ex", function (str, options) {
		var reg = /\{\{.*?\}\}/g;
		var result = false;
		var variables = str.match(reg);
		var context = this;
		_.each(variables, function (v) {
			var key = v.replace(/{{|}}/g, "");
			var value = typeof eval(`context.${key}`) === "string" ? `"${eval(`context.${key}`)}"` : eval(`context.${key}`);
			str = str.replace(v, value);
		});
		try {
			result = eval(str);
			if (result) {
				return options.fn(this);
			} else {
				return options.inverse(this);
			}
		} catch (e) {
			console.log(str, "--Handlerbars Helper \"ex\" deal with wrong expression!");
			return options.inverse(this);
		}
	});
	
	hbs.registerHelper("run", function (str) {
		var reg = /\{\{.*?\}\}/g;
		var variables = str.match(reg);
		var context = this;
		_.each(variables, function (v) {
			var key = v.replace(/{{|}}/g, "");
			var value = eval(`context.${key}`);
			str = str.replace(v, value);
		});
		return eval(str);
	});
	
};
