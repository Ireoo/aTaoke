const fs = require("fs");
const path = require("path");

const {configSet, JSONLength, ROOT, time, exec, decrypt} = require("getlibs.io")(path.join(__dirname, ".."));

// use plugin
// const getItem = require(path.join(ROOT, 'PLUGIN', 'taobao.items'));
// const getPoly = require(path.join(ROOT, 'PLUGIN', 'taobao.polys'));
const getSystem = require(path.join(ROOT, "PLUGIN", "system"));

let users = {};
let admins = {};
let status = {
	taobaoItem: false,
	taobaoPoly: false,
	taobaoKu: false,
	TBK: false
};

exports = module.exports = app => {
	const IO = require("socket.io")(app);
	
	getSystem(admins);
	
	IO.on("connection", socket => {
		
		// 前端用户浏览网页
		socket.on("user", (id, cb) => {
			if (id && users[id]) {
				cb(id);
			} else {
				users[socket.id] = socket;
				cb(socket.id);
			}
			IO.emit("users", JSONLength(users));
		});
		
		// 后端管理者连接
		socket.on("admin", cb => {
			cb({users: JSONLength(users), status});
			admins[socket.id] = socket;
		});
		
		// 用户推出登陆
		socket.on("disconnect", () => {
			delete users[socket.id];
			delete admins[socket.id];
			for (let a in admins) {
				admins[a].emit("users", JSONLength(users));
			}
		});
		
		// 获取淘宝商品列表
		socket.on("command", command => {
			
			eval(`status.${command} = true`);
			exec(admins, path.join(ROOT, "PLUGIN", `${command}.js`))
				.then(data => {
					eval(`status.${command} = false`);
					for (let a in admins) {
						admins[a].emit("console", {
							type: command,
							status: "warn",
							success: true,
							message: `程序运行结束!`,
							timer: time.format()
						});
					}
					for (let a in admins) {
						admins[a].emit(`command`, command, eval(`status.${command}`));
					}
				})
				.catch(err => {
					console.log(err);
					eval(`status.${command} = false`);
					for (let a in admins) {
						admins[a].emit("console", {
							type: command,
							status: "error",
							success: false,
							message: `程序运行出错!错误代码：${err}`,
							timer: time.format()
						});
					}
					for (let a in admins) {
						admins[a].emit(`command`, command, eval(`status.${command}`));
					}
				});
			
		});
		
		
		// 获取配置文件
		socket.on("config", cb => {
			let config = JSON.parse(decrypt(fs.readFileSync(path.join(process.cwd(), "DATA/.config"), "utf-8"), "iTaoke"));
			cb(config);
		});
		
		// 设置配置文件
		socket.on("config.set", (config, cb) => {
			cb(configSet(config));
		});

		socket.on("tbk", () => {
			console.log('start tbk')
			require(path.join(ROOT, "PLUGIN", `TBK.js`)).login();
		})
		
	});
	
};
