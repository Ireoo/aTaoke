// load basic ini
const fs = require("fs");
const path = require("path");

const {decrypt} = require("getlibs.io")(path.join(__dirname, ".."));

exports = module.exports = app => {

	app.use((req, res, next) => {
		let config = JSON.parse(decrypt(fs.readFileSync(path.join(process.cwd(), "DATA/.config"), "utf-8"), "iTaoke"));

		// 设置站点内容
		let address = req._parsedUrl.pathname === "/" ? "/index" : req._parsedUrl.pathname;
		let title = config.site.address[address] ? config.site.address[address] : "";
		let site = JSON.parse(JSON.stringify(config.site));
		site.title = `${title} ${site.name}`;
		site.url = req._parsedUrl.pathname;

		// 设置默认主题
		let themeDir = "default";
		if (site.theme && site.theme !== "") themeDir = site.theme;

		req.site = site;
		req.themeDir = themeDir;

		next();
	});

	// 用户登陆验证
	const auth = (req, res, next) => {
		if (!req.session.user) {
			res.redirect(301, `/login?url=${req.originalUrl}`);
		} else {
			next();
		}
	};

	// 加载文件
	const load = (dir, before, file) => {
		let p = file.split(".")[0];
		let r = require(path.join(dir, p));
		let l = r.path ? r.path : `${before}${p}`;
		delete r.path;
		let _type = [];
		if (r.auth) {
			delete r.auth;
			for (let type in r) {
				app[type](l, auth, r[type]);
				_type.push(type);
			}
			console.log(`通过 ${_type.join(', ')} 方式加载 ~/WEB/route${before}${file}, 挂载点 ${l}, 需要认证!`);
		} else {
			for (let type in r) {
				app[type](l, r[type]);
				_type.push(type);
			}
			console.log(`通过 ${_type.join(', ')} 方式加载 ~/WEB/route${before}${file}, 挂载点 ${l}, 不需要认证!`);
		}
	};

	// 扫描目录
	const scan = (dir, before = "/") => {
		let dirList = fs.readdirSync(dir);
		dirList.forEach(file => {
			let stat = fs.lstatSync(path.join(dir, file));
			if (!stat.isDirectory()) {
				load(dir, before, file);
			} else {
				scan(path.join(dir, file), `${before}${file}/`);
			}
		});
	};

	// 执行主体
	scan(path.join(__dirname, "../WEB/route"));

};
