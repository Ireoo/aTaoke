// load basic ini
const fs = require("fs");
const path = require("path");

const {config, ROOT} = require("getlibs.io")(path.join(__dirname, ".."));

// load web modules
const express = require("express");
const morgan = require("morgan");
const compression = require("compression");
const bodyParser = require("body-parser");
const session = require("express-session");
const FileStore = require("session-file-store")(session);
const md5 = require("md5");

exports = module.exports = (app, hbs) => {
	// show log into console
	app.use(morgan("[:date[iso]] :remote-addr[:remote-user] \":method :url HTTP/:http-version\" :status :res[content-length] \":referrer\" \":user-agent\" - :response-time ms"));

	// add gzip into web
	app.use(compression());

	/**
	 * 处理数据流成POST数据
	 */
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({extended: true}));

	/**
	 * 设置默认网页路径，并设置网页后缀
	 */
	app.set("views", path.join(ROOT, "WEB/html"));
	app.engine("hbs", hbs.__express);
	app.set("view engine", "hbs");

	/**
	 * 设置include文件的路径
	 */
	app.use(express.static(path.join(ROOT, "WEB/static")));

	/**
	 * 不缓存页面
	 */
	app.use((req, res, next) => {
		res.header("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		res.header("Pragma", "no-cache"); // HTTP 1.0.
		res.header("Expires", "0"); // Proxies.

		res.header("X-Powered-By", "aTaoke");

		next();
	});

	/**
	 * 全局处理，比如验证key等信息
	 */
	app.use((req, res, next) => {
		if (config.basic && config.basic.host && config.basic.host !== "") {
			if (req.headers.host === config.basic.host) {
				next();
			} else {
				res.status(404).send("NOT FOUND!!!");
			}
		} else {
			next();
		}
	});

	/**
	 * 添加session
	 */
	app.use(session({
		name: "aTaoke",
		secret: md5((new Date()).getTime()), // 用来对session id相关的cookie进行签名
		store: new FileStore(), // 本地存储session（文本文件，也可以选择其他store，比如redis的）
		saveUninitialized: true, // 是否自动保存未初始化的会话，建议false
		resave: true, // 是否每次都重新保存会话，建议false
		cookie: {
			maxAge: 1000 * 60 * 60 * 24 * 7 // 有效期，单位是毫秒
		}
	}));

	/**
	 * 设置session
	 */
	// app.use(session({
	// 	rolling: true,
	// 	secret: md5((new Date()).getTime()),
	// 	name: "aTaoke", //这里的name值得是cookie的name，默认cookie的name是：connect.sid
	// 	cookie: {maxAge: 60 * 1000 * 15}, //设置maxAge是80000ms，即80s后session和相应的cookie失效过期
	// 	resave: true,
	// 	saveUninitialized: true
	// }));
};
