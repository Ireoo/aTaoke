// load basic ini
const fs = require("fs");
const path = require("path");
const { DB } = require("getlibs.io")(path.join(__dirname, "../.."));

const md5 = require("md5");

// 主体程序
exports = module.exports = {
	get: (req, res, next) => {
		if (req.session.user) {
			return res.redirect(301, "/");
		}
		res.render(`${req.themeDir}/login`, { site: req.site });
	},
	post: async (req, res, next) => {
		let { username, password } = req.body;
		if (username === "" || password === "") {
			return res.send({ success: false, code: "用户名或者密码没有填写!" });
		}
		const db = new DB();
		password = md5(password);
		let r = await db
			.table("users")
			.where({ username, password })
			.getSync("findone");
		if (r.success) {
			let user = r.data;
			if (user.toString() === "") {
				return res.send({
					success: false,
					code: "该用户不存在!请确认后再提交!"
				});
			}
			delete user.password;
			req.session.user = user;
			res.send({
				success: true,
				data: user
			});
		} else {
			res.send(r);
		}
	}
};
