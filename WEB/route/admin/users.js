// load basic ini
const fs = require("fs");
const path = require("path");
const {DB} = require("getlibs.io")(path.join(__dirname, "../../.."));

// 主体程序
exports = module.exports = {
	get: async (req, res, next) => {
		const db = new DB();

		let users = await db.table("users").getSync("find");
		// res.send("admin/users");
		res.render(`${req.themeDir}/admin/users`, {site: req.site, session: req.session, users});
	},
	post: (req, res, next) => {
		// res.send("admin/users");
		res.render(`${req.themeDir}/admin/users`, {site: req.site, session: req.session});
	},
	auth: true
};
