// load basic ini
const fs = require("fs");
const path = require("path");
const {DB} = require("getlibs.io")(path.join(__dirname, "../../.."));

// 主体程序
exports = module.exports = {
	get: async (req, res, next) => {
		let db = new DB();

		let page = req.query.page || 1;
		let items = await db.table("items").other({skip: (page - 1) * 12, limit: 12}).getSync("find");
		let items_count = await db.table("items").getSync("count");
		let tbk_count = await db.table("items").where({click_url: {$ne: null}}).getSync("count");

		res.render(`${req.themeDir}/admin/items`, {
			site: req.site,
			session: req.session,
			items: items.data,
			items_count: items_count.data,
			tbk_count: tbk_count.data,
			page
		});
	},
	post: async (req, res, next) => {
		let db = new DB();

		let page = req.body.page;
		let items = await db.table("items").other({skip: (page - 1) * 12, limit: 12}).getSync("find");

		res.send(items);
	},
	auth: true
};
