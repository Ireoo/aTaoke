// load basic ini
const fs = require("fs");
const path = require("path");
const {DB, time} = require("getlibs.io")(path.join(__dirname, "../../.."));

// 主体程序
exports = module.exports = {
	get: async (req, res, next) => {
		const db = new DB();
		let data = {};

		data.gather = await db.table("gather").getSync("find");


		// get cookies
		let cookies = "";
		if (!fs.existsSync(path.join(process.cwd(), "DATA"))) fs.mkdirSync(path.join(process.cwd(), "DATA"));
		if (fs.existsSync(path.join(process.cwd(), "DATA", "cookies.json"))) {
			cookies = fs.readFileSync(path.join(process.cwd(), "DATA", "cookies.json"), "utf-8");
		}

		// res.send("admin/gather");
		res.render(`${req.themeDir}/admin/console`, {site: req.site, session: req.session, data, cookies});
	},
	post: async (req, res, next) => {
		const db = new DB();
		let data = {};

		switch (req.query.mode) {
			case "gatherAdd":
				data.gather = await db.table("gather").where({keyword: req.body.search}).data({$set: {timer: time.now()}}).other({upsert: true}).getSync("update");
				break;

			case "gatherDel":
				data.gather = await db.table("gather").where({keyword: req.body.search}).getSync("remove");
				break;
		}

		res.send(data);
	},
	auth: true
};
