// load basic ini
const fs = require("fs");
const path = require("path");
const {DB} = require("getlibs.io")(path.join(__dirname, "../../.."));

// 主体程序
exports = module.exports = {
	get: (req, res, next) => {
		// res.send("admin/setting");
		res.render(`${req.themeDir}/admin/setting`, {site: req.site, session: req.session});
	},
	post: (req, res, next) => {
		// res.send("admin/setting");
		res.render(`${req.themeDir}/admin/setting`, {site: req.site, session: req.session});
	},
	auth: true
};
