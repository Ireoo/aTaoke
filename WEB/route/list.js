// load basic ini
const fs = require("fs");
const path = require("path");
const {DB} = require("getlibs.io")(path.join(__dirname, "../.."));

// 主体程序
exports = module.exports = {
	get: async (req, res, next) => {
		site.title = `搜索 “${req.query.s}” 相关的宝贝 - ${site.name}`;
		try {
			let db = new DB();
			let page = req.query.page || 1, keyword;
			if (req.query.s) {
				keyword = {
					$regex: req.query.s
				};
			} else {
				keyword = {
					$ne: null
				};
			}
			let items = await db.table("items").where({title: keyword, status: 1}).other(
				{
					skip: (page - 1) * 100,
					limit: 100,
					sort: {
						volume: -1,
						tk_rate: -1
					},
					show: {
						_id: 1,
						pict_url: 1,
						title: 1,
						zk_final_price: 1,
						volume: 1,
						nick: 1,
						provcity: 1
					}
				}
			).getSync("find");
			let hots = await db.table("items").where({click_url: {"$ne": null}}).other(
				{
					limit: 10,
					sort: {
						tk_rate: -1,
						count: -1
					},
					show: {
						_id: 1,
						pict_url: 1,
						title: 1,
						zk_final_price: 1,
						volume: 1,
						nick: 1,
						provcity: 1
					}
				}
			).getSync("find");
			// res.send("list");
			res.render(`${req.themeDir}/list`, {site: req.site, session: req.session, items, hots, s: req.query.s});
		} catch (err) {
			res.status(500).send(err);
		}
	},
	post: async (req, res, next) => {
		try {
			let db = new DB();
			let page = req.query.page || 1, keyword;
			if (req.query.keyword) {
				keyword = {
					$regex: req.query.keyword
				};
			} else {
				keyword = {
					$ne: null
				};
			}
			let items = await db.table("items").where({title: keyword, status: 1}).other(
				{
					skip: (page - 1) * 100,
					limit: 100,
					sort: {
						volume: -1,
						tk_rate: -1
					},
					show: {
						_id: 1,
						pict_url: 1,
						title: 1,
						zk_final_price: 1,
						volume: 1,
						nick: 1,
						provcity: 1
					}
				}
			).getSync("find");
			res.send(items);
		} catch (err) {
			res.status(500).send(err);
		}
	}
};
