// load basic ini
const fs = require("fs");
const path = require("path");
const {DB} = require("getlibs.io")(path.join(__dirname, "../.."));

// 主体程序
exports = module.exports = {
	all: async (req, res, next) => {
		let db = new DB();

		let user_counts = await db.table("users").getSync("count");
		let tbk_counts = await db.table("items").where({click_url: {$ne: null}}).getSync("count");
		let item_counts = await db.table("items").getSync("count");
		// res.send("admin");
		res.render(`${req.themeDir}/admin`, {
			site: req.site,
			session: req.session,
			user_counts: user_counts.data,
			tbk_counts: tbk_counts.data,
			item_counts: item_counts.data
		});
	},
	auth: true
};
