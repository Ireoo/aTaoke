// load basic ini
const fs = require("fs");
const path = require("path");
const { DB } = require("getlibs.io")(path.join(__dirname, '../..'));

// 主体程序
exports = module.exports = {
    all: (req, res, next) => {
        // res.send("install");
        res.render(`${req.themeDir}/install`, {site: req.site});
    }
};
