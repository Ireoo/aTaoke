// load basic ini
const fs = require("fs");
const path = require("path");
const {DB} = require("getlibs.io")(path.join(__dirname, "../.."));

const md5 = require("md5");

// 主体程序
exports = module.exports = {
	get: (req, res, next) => {
		if (req.session.user) {
			return res.redirect(301, "/");
		}
		res.render(`${req.themeDir}/reg`, {site: req.site});
	},
	post: async (req, res, next) => {
		let {username, password, repassword} = req.body;
		if (username === "" || password === "") {
			return res.send({success: false, code: "用户名或者密码没有填写!"});
		}
		if (password !== repassword) {
			return res.send({success: false, code: "两次密码不一致!"});
		}
		const db = new DB();
		password = md5(password);
		let r = await db.table("users").data({username, password}).getSync("insert");
		if(r.success) {
			let user = r.data;
			if (user.op && user.op.username && user.op.username === username) {
				return res.send({
					success: false,
					code: "手机号已经存在!"
				});
			}
			delete user.data.password;
			req.session.user = user;
			res.send({
				success: true,
				data: user
			});
		} else {
			res.send(r);
		}
	}
};
