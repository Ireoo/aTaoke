// 主体程序
exports = module.exports = {
	get: (req, res, next) => {
		delete req.session.user;
		res.redirect("/");
	}
};
