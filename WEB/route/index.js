// load basic ini
const fs = require("fs");
const path = require("path");
const {DB} = require("getlibs.io")(path.join(__dirname, "../.."));

// 主体程序
exports = module.exports = {
	path: "/",
	all: async (req, res, next) => {
		let db = new DB();

		let page = req.query.page || 1;
		let items = await db.table("items").where({status: 1}).other(
			{
				skip: (page - 1) * 8,
				limit: 8,
				sort: {volume: -1},
				show: {
					_id: 1,
					pict_url: 1,
					title: 1,
					zk_final_price: 1,
					volume: 1,
					nick: 1,
					provcity: 1
				}
			}
		).getSync("find");
		let hots = await db.table("items").where({click_url: {"$ne": null}}).other(
			{
				limit: 10,
				sort: {tk_rate: -1, count: -1},
				show: {
					_id: 1,
					pict_url: 1,
					title: 1,
					zk_final_price: 1,
					volume: 1,
					nick: 1,
					provcity: 1
				}
			}
		).getSync("find");
		// res.send("index");
		res.render(`${req.themeDir}/index`, {
			site: req.site,
			session: req.session,
			items: items.data,
			hots: hots.data,
			s: req.query.s || "手机"
		});
	}
};
