function page(url, cb) {
	$("a.page").click(() => {
		var page = $(this).attr("data");
		$.ajax({
			url: url,
			type: "post",
			data: {
				page: page
			},
			dataType: "json",
			success: (data) => {
				cb(data);
			},
			error: () => {

			}
		});
		return false;
	});
}

function list_config(main, config, bef = "window.config") {
	for (let c in config) {
		let v = config[c];
		switch (typeof v) {
			case "string":
				var div = $("<div />").addClass("row").appendTo(main);
				var label = $("<div />").addClass("row-2").appendTo(div);
				var input = $("<div />").addClass("row-10").appendTo(div);
				$("<label />").addClass("from").text(c).appendTo(label);
				$("<input />").addClass("config").attr("data", `${bef}.${c}`).val(v).appendTo(input).change(function () {
					console.log($(this).attr("data"), $(this).val());
					eval(`${$(this).attr("data")} = "${$(this).val()}"`);
				});
				break;

			case "number":
				var div = $("<div />").addClass("row").appendTo(main);
				var label = $("<div />").addClass("row-2").appendTo(div);
				var input = $("<div />").addClass("row-10").appendTo(div);
				$("<label />").addClass("from").text(c).appendTo(label);
				$("<input />").addClass("config").attr("data", `${bef}.${c}`).val(v).appendTo(input).change(function () {
					console.log($(this).attr("data"), $(this).val());
					eval(`${$(this).attr("data")} = ${$(this).val()}`);
				});
				break;

			case "array":

				break;

			case "object":
				var div = $("<div />").addClass("row").appendTo(main);
				var h1 = $("<div />").addClass("row-12").appendTo(div);
				var _h1 = $("<h1 />").append($("<i />").addClass("fa").addClass("fa-angle-double-down")).append(c).appendTo(h1);
				list_config($("<div />").addClass("li").appendTo(main), v, `${bef}.${c}`);
				break;

			case "boolean":
				var div = $("<div />").addClass("row").appendTo(main);
				var label = $("<div />").addClass("row-2").appendTo(div);
				var input = $("<div />").addClass("row-10").appendTo(div);
				$("<label />").addClass("from").text(c).appendTo(label);
				var i = $("<input />").addClass("config").attr("type", "hidden").attr("data", `${bef}.${c}`).val(v).appendTo(input).change(function () {
					console.log($(this).attr("data"), $(this).val());
					eval(`${$(this).attr("data")} = ${$(this).val()}`);
				});
				var checkbox = $("<div />").attr("class", "checkbox").click(() => {
					var parent = $(this).parent();
					parent.addClass("on");
					console.log($(this).parent().hasClass("on"));
					if (checkbox.hasClass("on")) {
						checkbox.removeClass("on");
						i.val(false);
					} else {
						checkbox.addClass("on");
						i.val(true);
					}
				}).appendTo(input);
				if (v) checkbox.addClass("on");
				var change = $("<span />").appendTo(checkbox);
				break;
		}
	}
}
