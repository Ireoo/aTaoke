// load basic ini
const fs = require('fs');
const path = require('path');
const { DB } = require('getlibs.io');

// 主体程序
exports = module.exports = {
    get: (req, res, next) => {
        // res.send("{file}");
        res.render(`${req.themeDir}/{file}`, { site: req.site });
    },
    post: (req, res, next) => {
        // res.send("{file}");
        res.render(`${req.themeDir}/{file}`, { site: req.site });
    },
    all: (req, res, next) => {
        // res.send("{file}");
        res.render(`${req.themeDir}/{file}`, { site: req.site });
    }
};
